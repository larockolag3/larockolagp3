from enum import unique
from django.db import models
from django.db.models.enums import TextChoices
from datetime import datetime

# Create your models here.
class Roles(models.Model):
    nombre=models.CharField("Nombre rol",max_length=20)
    def __str__(self):
        return self.nombre

class Usuarios(models.Model):
    correo=models.EmailField("correo de usuario",unique=True, max_length=100)
    rol=models.ForeignKey(Roles,on_delete=models.CASCADE)
    nombres=models.CharField("Nombre de Usuario",max_length=100)
    apellidos=models.CharField("Apellidos  de Usuario",max_length=100)
    telefono=models.CharField("Telefono de Usuario",max_length=10)
    password=models.CharField("Contraseña",max_length=30)
    fechaRegistro=models.DateTimeField("Fecha de registro",auto_now_add=True,auto_now=False)
    fechaActualizacion=models.DateTimeField("Fecha en que se actualizo",auto_now_add=True,auto_now=False)
    def __str__(self):
        return self.apellidos + self.nombres

class Formatos(models.Model):
    nombre=models.CharField("Nombre del formato",max_length=20)
    def __str__(self):
        return self.nombre

class Generos(models.Model):
    nombre=models.CharField("Nombre del genero",max_length=20)
    def __str__(self):
        return self.nombre

class Interpretes(models.Model):
    nombre=models.CharField("Nombre del genero",max_length=20)
    def __str__(self):
        return self.nombre

class Autores(models.Model):
    nombres=models.CharField("Nombre de interprete",max_length=100)
    apellidos=models.CharField("Apellidos  de interprete",max_length=100)
    telefono=models.CharField("Telefono de interprete",max_length=10)
    f_Nacimiento=models.DateField("Fecha de nacimiento rinterprete")
    def __str__(self):
        return self.apellidos +" "+ self.nombres
   
class Canciones(models.Model):
    usuarioRegistro=models.ForeignKey(Usuarios,on_delete=models.CASCADE)
    autor=models.ForeignKey(Autores,on_delete=models.CASCADE)
    formato=models.ForeignKey(Formatos,on_delete=models.CASCADE)
    genero=models.ForeignKey(Generos,on_delete=models.CASCADE)
    interprete=models.ForeignKey(Interpretes,on_delete=models.CASCADE)
    nombre=models.CharField("Título de la canción",max_length=30)
    fechaRegistro=models.DateTimeField("Fecha de registro",auto_now_add=True,auto_now=False)
    fechaActualizacion=models.DateTimeField("Fecha en que se actualizo",auto_now_add=True,auto_now=False)
    def __str__(self):
        return self.nombre +" "+ self.genero
   #Despues de crear los modelos debemos registralos en admin.py 