from django.contrib import admin
from modulos.modCanciones.models import *
# Register your models here.
#Registrar las diferentes modelos de tablas creadas en models.py


admin.site.register(Roles)
admin.site.register(Usuarios)
admin.site.register(Formatos)
admin.site.register(Generos)
admin.site.register(Interpretes)
admin.site.register(Autores)
admin.site.register(Canciones)
