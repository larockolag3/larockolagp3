from django.apps import AppConfig


class ModcancionesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'modulos.modCanciones'
